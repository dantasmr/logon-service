package com.dantasmr.logon.service;

import java.net.SocketTimeoutException;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.dantasmr.logon.exceptions.ConnectionException;
import com.dantasmr.logon.exceptions.TimeOutLogonException;
import com.dantasmr.logon.model.Usuario;

@Service
public class WebTargetService {

	
	private static final String TOKEN_FIELD = "x-access-token";

	@Autowired
	private ResteasyClient resteasyClient;

	public Response getResponse(String endpoint) throws TimeOutLogonException, ConnectionException {
		return getResponse(endpoint, null, null);
	}

	public Response getResponse(String endpoint, Usuario usuario)
			throws TimeOutLogonException, ConnectionException {
		return getResponse(endpoint, usuario, null);
	}

	public Response getResponse(String endpoint, String token)
			throws TimeOutLogonException, ConnectionException {
		return getResponse(endpoint, null, token);
	}

	private Response getResponse(String endpoint, Usuario usuario, String token)
			throws TimeOutLogonException, ConnectionException {

		Response response;

		try {
			ResteasyWebTarget target = resteasyClient.target(endpoint);
			if (usuario != null) {
				response = target.request().post(Entity.entity(usuario, MediaType.APPLICATION_JSON));
			} else if (!StringUtils.isEmpty(token)) {
				response = target.request().header(TOKEN_FIELD, token).get();
			} else {
				response = target.request().get();
			}

		} catch (ProcessingException pr) {
			Throwable cause = pr.getCause();
			if (cause instanceof SocketTimeoutException) {
				throw new TimeOutLogonException();
			} else {
				throw new ConnectionException();
			}

		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				throw new TimeOutLogonException();
			}
			e.printStackTrace();
			throw new ConnectionException();
		}

		return response;

	}

}
