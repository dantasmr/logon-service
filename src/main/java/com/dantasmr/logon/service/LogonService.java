package com.dantasmr.logon.service;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dantasmr.logon.exceptions.ConnectionException;
import com.dantasmr.logon.exceptions.InvalidLogonException;
import com.dantasmr.logon.exceptions.ProdutosException;
import com.dantasmr.logon.exceptions.TimeOutLogonException;
import com.dantasmr.logon.exceptions.UserInfoException;
import com.dantasmr.logon.model.HostApi;
import com.dantasmr.logon.model.Token;
import com.dantasmr.logon.model.UserInfo;
import com.dantasmr.logon.model.Usuario;

@Service
public class LogonService {

	private static final int SUCCESS = 200;
	private final static String ENDPOINT_HOSTNAME = "http://localhost:3500/config/hostname";
	private static final String ENDPOINT_AUTH = "/gateway/sts/v1/oauth/token";
	private static final String ENDPOINT_USERINFO = "/gateway/cliente/v1/clientes/";
	private static final String ENDPOINT_PRODUTOS = "/gateway/produto/v1/produtos/";
	
	@Autowired
	private WebTargetService webTargetService;

	
	private String getHostName(String endpoint, String param) throws TimeOutLogonException, ConnectionException {
		Response response = webTargetService.getResponse(ENDPOINT_HOSTNAME);
		HostApi hostapi = response.readEntity(HostApi.class);
		response.close();
		return hostapi.getHostname() + endpoint + param;	

	}
	

	public Token getToken(Usuario usuario) throws InvalidLogonException, TimeOutLogonException, ConnectionException {
		Token token;
		String endpoint = getHostName(ENDPOINT_AUTH, "");
		Response response = webTargetService.getResponse(endpoint, usuario);
		int status = response.getStatus();
		if (status != SUCCESS) {
			response.close();
			throw new InvalidLogonException(status);
		}else {
			token = response.readEntity(Token.class);
			response.close();
		}
		
		return token;
	}
	
	
	public UserInfo getUserInfo(String token, String nome) throws UserInfoException, TimeOutLogonException, ConnectionException {
		
		UserInfo userinfo;
		String endpoint = getHostName(ENDPOINT_USERINFO, nome);
		Response response = webTargetService.getResponse(endpoint, token);
		
		int status = response.getStatus();
		if (status != SUCCESS) {
			response.close();
			throw new UserInfoException();
		}else {
			userinfo = response.readEntity(UserInfo.class);
			response.close();
		}
		
		return userinfo;
		
	}
	
	public String[] getUserProdutos(String token, String nome) throws ProdutosException, TimeOutLogonException, ConnectionException {
		String[] produtos;
		String endpoint = getHostName(ENDPOINT_PRODUTOS, nome);
		Response response = webTargetService.getResponse(endpoint, token);
		int status = response.getStatus();
		if (status != SUCCESS) {
			response.close();
			throw new ProdutosException();
		}else {
			produtos = response.readEntity(String[].class);
			response.close();
		}
		
		return produtos;
		
	}
}