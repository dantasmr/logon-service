package com.dantasmr.logon.exceptions;

public class InvalidLogonException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -111743832699482193L;
	private int statusCode;
	
	public InvalidLogonException(int statusCode) {
		super();
		this.statusCode = statusCode;
	}

	
	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	
}
