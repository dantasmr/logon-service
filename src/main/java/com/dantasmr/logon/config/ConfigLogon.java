package com.dantasmr.logon.config;

import java.util.concurrent.TimeUnit;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigLogon {

	@Bean
	public ResteasyClient resteasyClient() {
		ResteasyClientBuilder clientBuilder = (ResteasyClientBuilder) ResteasyClientBuilder.
				newBuilder()
				.register(ResteasyJackson2Provider.class);
		clientBuilder.connectTimeout(15, TimeUnit.SECONDS);
		clientBuilder.readTimeout(15, TimeUnit.SECONDS);
		return clientBuilder.build();
	}

}
