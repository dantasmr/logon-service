package com.dantasmr.logon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dantasmr.logon.exceptions.ConnectionException;
import com.dantasmr.logon.exceptions.InvalidLogonException;
import com.dantasmr.logon.exceptions.ProdutosException;
import com.dantasmr.logon.exceptions.TimeOutLogonException;
import com.dantasmr.logon.exceptions.UserInfoException;
import com.dantasmr.logon.model.Cliente;
import com.dantasmr.logon.model.ErroMsg;
import com.dantasmr.logon.model.Token;
import com.dantasmr.logon.model.UserInfo;
import com.dantasmr.logon.model.Usuario;
import com.dantasmr.logon.service.LogonService;

@RestController
@RequestMapping("user")
public class Controller {

	@Autowired
	private LogonService logonService;

	@PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> login(@RequestBody Usuario usuario) {

		ResponseEntity<Object> resu;

		try {
			
			Token token = logonService.getToken(usuario);
			UserInfo userInfo = logonService.getUserInfo(token.getAccessToken(), usuario.getUsuario());
			String[] produtos = logonService.getUserProdutos(token.getAccessToken(), usuario.getUsuario());

			Cliente cliente = new Cliente();
			cliente.setNome(userInfo.getNome());
			cliente.setEndereco(userInfo.getEndereco());
			cliente.setTelefone(userInfo.getTelefone());
			cliente.setProdutos(produtos);

			resu = new ResponseEntity<Object>(cliente, HttpStatus.OK); 
			
		} catch (InvalidLogonException ile) {
			ErroMsg erroMsg = new ErroMsg("001", "Usuario ou senha invalidos");
			resu = new ResponseEntity<Object>(erroMsg, HttpStatus.FORBIDDEN);			
		} catch (UserInfoException ile) {
			ErroMsg erroMsg = new ErroMsg("002", "Usuario nao localizado");
			resu = new ResponseEntity<Object>(erroMsg, HttpStatus.NOT_FOUND); 
		} catch (ProdutosException ile) {
			ErroMsg erroMsg = new ErroMsg("003", "Produtos nao localizados");
			resu = new ResponseEntity<Object>(erroMsg, HttpStatus.NOT_FOUND);
		} catch (TimeOutLogonException ile) {
			ErroMsg erroMsg = new ErroMsg("004", "Timeout gateway");
			resu = new ResponseEntity<Object>(erroMsg, HttpStatus.GATEWAY_TIMEOUT);
		} catch (ConnectionException tout) {
			ErroMsg erroMsg = new ErroMsg("005", "gateway nao responde");
			resu = new ResponseEntity<Object>(erroMsg, HttpStatus.BAD_GATEWAY); 
		}

		return resu;

	}

}
