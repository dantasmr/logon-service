package com.dantasmr.logon.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HostApi {

	
	@JsonProperty("hostname")
	private String hostname;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	
	
	
	
}
