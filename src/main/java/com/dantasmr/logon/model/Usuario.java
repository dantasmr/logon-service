package com.dantasmr.logon.model;

import java.io.Serializable;

public class Usuario implements Serializable{
	
	private static final long serialVersionUID = 5356057107590462568L;

	public Usuario() {}

	private String usuario;
	private String senha;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
	
	
}
