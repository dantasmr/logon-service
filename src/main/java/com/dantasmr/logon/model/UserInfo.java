package com.dantasmr.logon.model;

import java.io.Serializable;

public class UserInfo  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2570326107974795918L;


	private String nome;
	private String endereco;
	private String telefone;
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	




}
