package com.dantasmr.logon.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Token implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4229944034235588807L;
	@JsonProperty("access_token")
	private String accessToken;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	
	
}
