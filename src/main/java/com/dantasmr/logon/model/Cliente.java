package com.dantasmr.logon.model;

import java.io.Serializable;

public class Cliente implements Serializable{
	
	private static final long serialVersionUID = -3219924922552739382L;
	
	private String nome;
	private String endereco;
	private String telefone;
	
	private String[] produtos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String[] getProdutos() {
		return produtos;
	}

	public void setProdutos(String[] produtos) {
		this.produtos = produtos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	

}
