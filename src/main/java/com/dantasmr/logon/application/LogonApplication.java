package com.dantasmr.logon.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.dantasmr.logon")
public class LogonApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogonApplication.class, args);
	}

}



