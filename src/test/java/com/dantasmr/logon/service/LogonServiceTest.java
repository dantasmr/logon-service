package com.dantasmr.logon.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import com.dantasmr.logon.application.LogonApplication;
import com.dantasmr.logon.model.Token;
import com.dantasmr.logon.model.UserInfo;
import com.dantasmr.logon.model.Usuario;

@SpringBootTest(classes = LogonApplication.class)
class LogonServiceTest {

	@Mock
	private LogonService logonService;
	
	private Usuario usuario;

	@BeforeEach
	public void setUp() {

		
		try {
			usuario = getUsuarioTeste();
			Token token = getTokenTeste();
			UserInfo userInfo = getUserInfoTeste();
			String[] produtos = getProdutosTeste();
		
			Mockito.when(logonService.getToken(usuario)).thenReturn(token);

			Mockito.when(logonService.getUserInfo(token.getAccessToken(), usuario.getUsuario())).thenReturn(userInfo);

			Mockito.when(logonService.getUserProdutos(token.getAccessToken(), usuario.getUsuario()))
					.thenReturn(produtos);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	void testgetToken() {
		
		try {
			Token token = logonService.getToken(usuario);
			assertNotNull(token);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Test
	void testgetUserInfo() {

		Usuario usuario = getUsuarioTeste();
		Token token = getTokenTeste();

		try {
			UserInfo userInfo = logonService.getUserInfo(token.getAccessToken(), usuario.getUsuario());
			assertNotNull(userInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Test
	void testgetUserProdutos() {

		Usuario usuario = getUsuarioTeste();
		Token token = getTokenTeste();

		try {
			String[] produtos = logonService.getUserProdutos(token.getAccessToken(), usuario.getUsuario());
			assertNotNull(produtos);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private Usuario getUsuarioTeste() {
		Usuario usuario = new Usuario();
		usuario.setUsuario("admin");
		usuario.setSenha("admin");
		return usuario;
	}
	
	private Token getTokenTeste() {
		Token token = new Token();
		token.setAccessToken("xxxxxxxx");
		return token;
	}
	
	private UserInfo getUserInfoTeste() {
		UserInfo userInfo = new UserInfo();
		userInfo.setNome("admin");
		userInfo.setEndereco("Rua xpto");
		userInfo.setTelefone("99999");
		return userInfo;
	}
	
	private String[] getProdutosTeste() {
		String[] produtos = { "produto1", "Produto2", "Produto3" };
		return produtos;
	}

}
